---
layout: markdown_page
title: "Webinars"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Webinars

Webinars are one the greatest tools we have to communicate with our audience. GitLab webcasts aim to be informative, actionable, and interactive. For a comprehensive overview on how to set up a webcast, please visit the [Business Operations section](/handbook/business-ops#Webinars) of the handbook.  
 
## Going Live on YouTube

We're happy to support teams who want to conduct meetings, brainstorms, demos etc. [live on YouTube](/handbook/tools-and-tips/#sts=Livestreaming to YouTube). This is a great way to practice our value of transparency and involve the community in work that often goes on "behind the scenes." A regular example of this is our live streamed kickoffs and retrospectives for each release.
