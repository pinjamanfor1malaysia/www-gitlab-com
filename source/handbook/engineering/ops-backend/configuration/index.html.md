---
layout: markdown_page
title: "Configuration Team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Configuration Team

The configuration team is responsible for developing features of GitLab that
relate to the "Configuration" and "Operations" stages of the DevOps lifecycle. These refer to 
configuration of infrastructure as well as running applications that are deployed via
GitLab.

This team currently maintains 
our integrations with Kubernetes including the [Auto DevOps](/auto-devops) feature
set.

As per the [product categories](/handbook/product/categories/) this team will
also be responsible for building out new feature sets around:

- ChatOps
- Serverless (or Functions As A Service)
